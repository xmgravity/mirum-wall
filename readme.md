# Mirum Wall

This is a utility server for Mirum Wall interactive light.

## Requirements

  * [Node.js](http://nodejs.org): Use the installer provided on the NodeJS website.
  * [Twit.js](https://github.com/ttezel/twit): Twitter API Client for node
  * [Node Hue API](https://github.com/peter-murray/node-hue-api): Node.js Library for interacting with the Philips Hue Bridge and Lights

## Quickstart

```bash
git clone git@bitbucket.org:xmgravity/mirum-wall.git
npm install
```
