// NodeJS Dependencies..
var http = require('http');
var Twit = require('twit');
var HueApi = require("node-hue-api").HueApi;
var lightState = require("node-hue-api").lightState;

// The IDs of all Philips Hue lamps
var lightIdsAll = [1, 2, 3, 4];

/**
 * NodeJS lowest common denominator setup
 */
http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end('Hello World\n');
}).listen(1337, '127.0.0.1');
console.log('Server running at http://127.0.0.1:1337/');

/**
 * Philips Hue API configuration
 */
var hostname = "10.159.137.48", //the Philips Hue bridge IP
    username = "newdeveloper",
    api;

api = new HueApi(hostname, username);

/**
 * Twitter Stream API configuration
 */
var T = new Twit({
    consumer_key:         '',
    consumer_secret:      '',
    access_token:         '',
    access_token_secret:  ''
});

var stream = T.stream('statuses/filter', { track: 'MakeMirum' });
var currentLampID = 0;
var previousLampID = 0;

/**
 * Change the color of Philips Hue lamp when a new tweet containing 'MakeMirum' is generated. The color is selected at random value, but the lamp is selected in ascending order. When no tweet is generated in the next 5 seconds, we will loop the color of Philips Hue lamp as the default set.
 */
stream.on('tweet', function (tweet) {
  previousLampID = currentLampID;
  currentLampID = (currentLampID % 4) + 1;
  var color = [Math.random(), Math.random()];
  api.setLightState(currentLampID, {"xy":color, "sat": 255, "bri":255, "effect": "colorloop"})
    .then(console.log("::", tweet.text, "::", currentLampID, color))
    .done();
  
  setTimeout(function() {
    api.setLightState(currentLampID, {"sat":100, "bri":100, "effect": "colorloop"}).done();
  }, 5000);
});


/**
 * Generate a rounded random integer in the range from {min} to {max} value.
 * @param {int} min The lowest bound of randomized value.
 * @param {int} max The highest bound of randomized value.
 * @return A rounded random integer.
 */
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}


